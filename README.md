# 30daysofnode
## Day 1 - All about servers
### Summary
- Serve a String using node.js server
- Serve html file using node.js server
- Serve JSON using node.js server
- Serve pdf file using node.js server
- Serve mp3 file using node.js server
- Serve video/mp4 file using node.js server

## Day 2 - File System
### Summary
- Read file
- Write File
- Append file
- Rename file
- Delete file
