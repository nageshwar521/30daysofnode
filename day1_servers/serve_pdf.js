// server_pdf.js
const http = require('http');
const fs = require('fs');

http.createServer((req, res) => {
  res.writeHead(200, {"Content-Type": "application/pdf"});
  fs.readFile("test.pdf", (err, data) => {
    if (err) {
      res.json({status: 'error', msg: err});
    } else {
      res.write(data);
      res.end();
    }
  });
}).listen(5000);
