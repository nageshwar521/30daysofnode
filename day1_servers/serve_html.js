// serve_html.js
const http = require('http');
const fs = require('fs');

http.createServer((req, res) => {
  res.writeHead(200, {"Content-Type": "text/html"});
  fs.readFile('index.html', (err, data) => {
    if (err) throw err;
    console.log("done");
    res.end(data);
  });
}).listen(5000);
