// simple_server.js
const http = require('http');

const host = '127.0.0.1';
const port = 5000;

const server = http.createServer((request, response) => {
  response.writeHead(200, { 'Content-Type': 'text/plain' });
  console.clear();
  console.log('headers');
  console.log(require('url').parse(request.url));
  response.end('server working success!');
});

server.listen(port, host, (error) => {
  if (error) {
    return console.log(`Error occured: ${error}`);
  }

  console.log(`server is listening on ${host}:${port}`);
})
