// serve_mp3.js
const http = require('http');
const fs = require('fs');

http.createServer((req, res) => {
  fs.exists('unnattundi_gundey.mp3', (exists) => {
    if (exists) {
      res.writeHead(200, {'Content-Type': 'audio/mp3'});
      const rstream = fs.createReadStream('unnattundi_gundey.mp3');
      rstream.on('data', (response) => {
        console.log('its streaming now...');
        console.log(arguments);
      });
      rstream.pipe(res);
    } else {
      res.writeHead(403, {'Content-Type': 'text/html'});
      res.write('404 not found!');
      res.end();
    }
  });
}).listen(5000);
