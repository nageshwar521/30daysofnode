// serve_mp4.js
const http = require('http');
const fs = require('fs');

http.createServer((req, res) => {
  res.writeHead(200, {'Content-Type': 'video/mp4'});
  fs.exists('unnattundi_gundey.mp4', (exists) => {
    if (exists) {
      const rstream = fs.createReadStream('unnattundi_gundey.mp4');
      rstream.pipe(res);
    } else {
      res.send('404');
      res.end();
    }
  });
}).listen(5000);
