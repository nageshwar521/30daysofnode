// server_json.js
const http = require('http');
const fs = require('fs');

http.createServer((req, res) => {
  res.writeHead(200, {"Content-Type":"application/json"});
  let json_response = {
    status: 200,
    message: 'successful',
    result: [ 'sunday', 'monday', 'tuesday' ],
    code: 2000
  };
  console.log('server running...');;
  res.end(JSON.stringify(json_response));
}).listen(5000);
