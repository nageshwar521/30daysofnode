// append_file_async.js
const fs = require('fs');

const newData = "this content will be appended at the end of the file";

fs.appendFile('append_file_async.txt', newData, (err) => {
  if (err) {
    console.log(err);
  } else {
    console.log("the new content appended successfully!");
  }
});
