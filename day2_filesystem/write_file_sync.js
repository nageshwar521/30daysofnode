// write_file_sync.js
const fs = require('fs');

const content = "This is the content of the file";
fs.writeFileSync('content.txt', content);
console.log("File written successfully!");
