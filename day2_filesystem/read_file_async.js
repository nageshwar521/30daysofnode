// read_file_async.js
const fs = require('fs');

fs.readFile('message.txt', (err, data) => {
  if (err) {
    console.log(err);
  } else {
    console.log("Content : ", data);
  }
});
