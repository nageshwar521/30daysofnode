// write_file_async.js
const fs = require('fs');

const content = "this is the content of the file!";
fs.writeFile('message.txt', content, (err) => {
  if (err) {
    console.log(err);
  } else {
    console.log('Its saved!');
  }
});
