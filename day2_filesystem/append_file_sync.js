// append_file_sync.js
const fs = require('fs');

const newData = "The new data appended synchronously at the end of the file";
fs.appendFileSync('append_file_sync.txt', newData);
console.log("File appended successfully");
